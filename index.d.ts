interface BreakPointsProp {
    breakpoints: {
        minWidth?: number
        minHeight?: number
        maxWidth?: number
        maxHeight?: number
    }
}

export function withBreakpoints<OwnProps>(Component: React.ElementType<OwnProps>): React.FC<OwnProps & BreakPointsProp>